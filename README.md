# README 

The requirements and the approach are communicated in this md file. Please read the following and build your code accordingly and pay attention to the following and read carefully: 

 - If you need to communicate with any of the project owners if you have any questions you can reach us immeditaly on slack channels.
 - We prefer if you assign yourself tasks on our project managenet tool called Taiga which is very similar to Jira. This way we can know about your plan and progress, and so we can advise you on priorities or approaches to make your life easier. 
 - We will have a sprint meeting for 30 to 60 minutes every 2 weeks so that you can reflect on what you have done, show us any demos if any, and discuss approaches or milestones. Ofcourse we will be available whenever you need us just ping us on slack and we can reach out to you as soon as possible.  
 - You are free to make your own work schedule but by the end of each work session, you are required to push your code everytime you have done some coding, that way we can have a backup and we can monitor your activity and do code reviews. 
 - Your repository is totally manged by you feel free to work whichever way you please, if you want to create your own branches thats ok or if you want to keep pushing in the master branch thats ok as well. However, you are requied to write us a readme.md file to describe how to run your code when it is ready to run, and describe the colomns of the output tables in redshift. 
 - To access our posgress SQL-redshift for the data please use the following credintials: 
 	Host: "retail-analytics.cmrxcu1tepas.us-west-2.redshift.amazonaws.com", port: "5439", DB Name: "iridataset", username: "oliver" password: "qnTT5+9*45eeW!jodP"	
 - To access S3 use the following keys, Access Key: "AKIAI42NJJWDBJXSWOEA", Key Pass: "s5i9cBGNixjjOl0mY5ijmQlwcjylW3lLK+PRMQMs"
 - You encourage you to use S3 to unload your exhaustive huge query into redshift if the size of the data is large, please refer to the DB.py file for an example and read the comments. 
 - Please note that your peers will be using the same access keys and redshift passwords so please do not delete any tables unless you let us know what you exactly want to delete. Unless you want to delete a table you have personally created yourself in case something went wrong then sure you can go ahead. 
 - The delivrables of the project is pretty much a bunch of sql tables as your code writes these tables. Please create your own schema before you create the tables, **DO NOT** add data or create tables in the public schema.
 - Please create a seperate schema for each usecase and call it for example "customer_segmentation_output"
 - Denote your schema name in conf.properties file as 'database.schema'
 - Please add a timestamp column to your output tables. 
 

Wish you the best of luck and remember don't think of us as if we are your boss, baisically we are here to help you achieve your best and learn new things from each other :)   

# Promotion Performance Prediction: 

## General Requirements and Objective:

Channel partner analytics and collaboration is our term for driving capabilities in improved planning and collaboration that is based on insights into channel partner sales, merchandising conditions and inventory. In particular, improvements in capabilities such as price, promotion and assortment planning and execution will drive significant bottom line results. Better managing store-level sales and category management reporting will improve visibility and better guide future plans. 

Consumer products companies continue to struggle with their inability to predict the impact of trade spend on lift, consumer behaviour and profit. They often lack the capability to evaluate the impact and profitability of past promotions, and cannot validate that trade events have been executed as planned. Because of disjointed planning processes and inaccurate forecasts, there is often suboptimal performance in sales and costs. 

## What the Stalk-holder needs ?

Improved and optimized plans for baseline and incremental volumes is required which is based on consumption data and predictive analytics. Being able to predict the impact of promotions on lift and profit, including the category, cross-category and cross-customer impact is essential. Additionally, evaluation of the impact and profitability of past promotions will reduce cost, improve ROI and inform future planning. 

Basically we need to answer two major questions: 
What are the kinds of promotions that works for a given type of product?
How does different types of promotions work for different categories ?
What are the factors that are associated with a successful promotion ?

## Dataset to Use:
store_data
store_demographics
product_attributes
External Events: http://api.eventful.com/docs/events/search 
Weather data: https://pypi.python.org/pypi/get-weather-data
economic data: https://www.quandl.com/data/FRED-Federal-Reserve-Economic-Data?keyword=

## How to do it ?  

Basically the Questions we want to answer are the following: 

- What criteria makes a promotion successful based on past promotions? Define success?
- What are the values of lift and profit for each past promotion? How can we predict the lift and profit of a future promotion given other criteria?
- What is an ideal length (in weeks) and price reduction for potential promotion for a specific item


### Step1, prepare your data: Transform the store_data into a new table that can be operated on via association analysis, regressive analysis and prediction.

Procedure:
There are 52 weeks per year. The description of the years tables is provided in a previous section. To summarize:

store_data basically includes the following columns: 
- item, # the item id (for ex colgate toothpaste)
- vend, # the vendor id (for ex walmart)
- store_id, # the key of a particular item (anonymized outlet value for ex the Walmart outlet on Story Road, San Jose, CA, USA)
- week, # the week associated with this entry
- sy, 
- ge,
- units, # total number of units sold of this item for specified week (this can be 0 if no items were sold - actually if the value is 0 the week entry does not show up in the table)
- dollars, # total number of sales dollars for that item during this week
- f, # the promo features
- d, # the promo display
- pr # a flag indicating if this week was a promotional week (1) or not (0)
			]

The ETL results in 2 tables: a promo table & a non-promo table.

The promo table has the following fields and it is called "promo_table_colums" :

- 'sample',
- 'item',
- 'vend',
- ‘store_id',
- 'promo_start_week', # we can compute (promo_end_week = promo_start_week + number_of_promo_weeks)
- 'promo_features',
- 'promo_display',
- 'avg_promo_unit_price', 
- 'number_of_promo_weeks', 
- 'number_of_datapoints', 
- 'total_units_sold_in_promo', 
- 'total_dollar_sales_value_in_promo', 
- 'number_of_weeks_in_pre_promo_window', # up to a max of 5
- 'number_of_weeks_in_post_promo_window', # up to a max of 5 
- 'total_units_sold_in_pre_promo_window',
- 'total_dollar_sales_in_pre_promo_window',
- 'total_units_sold_in_post_promo_window',
- 'total_dollar_sales_in_post_promo_window',
- 'promo_profit',
- 'promo_lift',
- 'md5hash' # we use this to uniquely identify this row in the table based on its field values - to avoid duplicates
                  

The non-promo table has the following fields and is called "non_promo_table_colums" :

- 'sample',
- 'item',
- 'vend',
- 'key', 
- 'avg_weekly_units_sold_non_promo_weeks', 
- 'avg_weekly_price_point_non_promo_weeks',
- 'md5hash'

The goal is to have 2 tables that summarize all the promotions across the 52 weeks of year 4. The steps involved to create them are:

1) Identify an item-vendor pair that has good potential for analysis (We explored the category of toothpaste: item='41351',vend='37000' as it seemed to have a large number of promotional weeks and would be a good candidate for an initial explorative analysis)
2) For a specific choice of (item,vendor) identify the promotions that occur throughout the years; Each promotion is characterized by a start week, an end week and some additional fields (see the promo table described above). Similarly we evaluate another table that summarizes the metrics associated with non-promotional weeks for the (item, vend) pair we selected - this is used to establish a baseline for our computations.

We identify a promotion week via the pr field in the years table. There may be missing week entries during a promotional period (if there are no sales during those weeks). We take that into account when computing the promo metrics. There a re several cases we need to consider. For simplicity lets symbolize a row belonging to a particular (item, vendor, store_id) tuple by WW_p where WW refers to the week number (ex 03 or 18) and p is either 0 (non-promo week) or 1 (promo week); there are a few scenarios to consider
A- No missing weeks:
... 03_0, 04_1, 05_1, 06_1, 07_1, 08_0, ..
This is the ideal case. It is an uninterrupted sequence of weeks for a particular (item, vendor, store_id) tuple. The start and end of a promotion is very clearly represented (start_of_promo = week 4, end_of_promo = week 7, promo duration = 4 weeks)

B- Missing weeks within promo period:
... 03_0, 04_1, 05_1, 07_1, 08_0, ..
Here week 6 is missing from the table as there were no sales during that week. We implicitly assume the promo also existed during week 6 since both weeks 5 & 7 were promo weeks. A such the start and end of a promotion are (start_of_promo = week 4, end_of_promo = week 7, promo duration = 4 weeks)


C- Missing weeks at the beginning of a promo:
... 03_0, 06_1, 07_1, 08_0, ..
Here weeks 4 & 5 are missing from the table as there were no sales during those weeks. We implicitly assume that no promo existed during those weeks and that the promo actually started on week 6. As such the start and end of a promotion are (start_of_promo = week 6, end_of_promo = week 7, promo duration = 2 weeks)


D- Missing weeks at the end of a promo:
... 03_0, 04_1, 05_1, 08_0, ..
Here weeks 6 & 7 are missing from the table as there were no sales during those weeks. We implicitly assume that no promo existed during those weeks and that the promo actually ended on week 5. As such the start and end of a promotion are (start_of_promo = week 4, end_of_promo = week 5, promo duration = 2 weeks)

E- A hybrid mix of cases B, C, and D

Note: we may be introducing an error in the promo metrics if the missing weeks were actually promo weeks - it would be best to make a note of promos that fall under cases C & D and we may decide to ignore them in the later machine learning analysis stages depending on how many days were missing (this could reduce our confidence in this particular datapoint). In our preliminary ETL script we haven’t included any indicators for missing data points at the beginning or end of a promotional period but it would be instructive to modify the code to include this.
The same argument can be made for case B especially if there are many missing weeks within a promo (we capture this case via the two fields 'number_of_promo_weeks' and 'number_of_datapoints' in the promo table)

We also maintain a pre & post promo window which is used to get some metrics associated with the periods of time directly before and after a particular promo.
We use a 5 week post and pre promo window although this number is somewhat arbitrary and hasn’t been optimized.
The assumption of a successful promo is that it results in increased sales during the promo and extends to the weeks directly following the promo. One parameter we are interested in estimating/predicting is the post promo window to see how long does the influence of a promo last after it has ended.

3) Compute the Lift & profit of each promotion:
- for each promo entry compute the price reduction by comparing the avg. price on non-promo days to the avg. price on promo days
- the promo's profit is the total money made during the promo minus the los due to the price reduction 
profit_during_promo_period = total_sales_in_promo_period - (number_of_promo_weeks * (NON_promo_price - promo_price) )
lift1 = total_sales_during_promo/total_prom_weeks - total_sales_during_non_promo_pre/post_window/non_promo_pre/post_window
if lift1 is big => avg. money made during promotional period brought in an increase in profit as opposed to avg. money made on non promo day => successful promo!
lift2 = total_sales_post_promo_window  - total_sales_pre_window 


### Important Notes:

* We assume all weeks not present in current year are missing
* We have written a script that achieves this and will be providing it for reference on the bitbucket account
We will need to account for seasonality in our analysis; seasonality is the natural increase and decrease in sales of particular items that are correlated to particular times of year (for ex increased sales of ice-cream in July); we also don’t account for special events that may result in increases in demand for products (such as increased demand for beer during the football finals or the world cup) that is why you will have to enrich the data set for each week by extracting some seasonal data using the External Events, weather data, and economics API. They are granular enough to get local events, weather, and economics data by date and geographic locations. 
Basically almost all step 1 has been done for you, please check the file “PromoAnalyzer.py” and you **need to carry on step one by doing the following**:

- You need to merge promo_table and non_promo_table with the store_demographics by store_id.  
- You need to merge promo_table and non_promo_table with the product_attribute by UPC or sy, ge, item, vend.
- Basically you need to merge all these data together in one table and you will  also enrich the table with more external data such as weather data, local events.
- After joining all these tables together you need to enrich both the promo_table, and non_promo_table with weather data using the weather data API. Basically you need to get the average, standard deviation, min, and max temperature for the week data of each designated area of the store for each store in store_data. 
- After that you need to enrich these 2 tables with events data. Extract local events such as sports, finance, economics, political events as well using the [events_api] , you need to create a random API key to extract the data once. You can use the [events_search] to search for local events by city, date, category, and geolocation. you will have to use the geolocations specified to the store for the years 2011 and 2012. As for events category use [events_category] to know the types and ids of events to use in your event search. Think of events that can directly affect grocery prices such as financial events, sports, economic events or news. 
- For all items sold you need to extract the fast Fourier transform features of the number of items sold for each unique item. make sure that the data is ordered in ascending order by week for each item separate. 

---------------------------------------
### Step2: Association Rules
In this step you need to create association analysis using A-priori algorithm to find the association rules of lift and profit using the feature set you have built in step 1. Basically we are trying to understand how the promotions work for each item among all stores. You will also need to repeat it but among item categories. Basically this is a summary of what needs to be done : 

The association rules should be performed on each unique item among all stores. The rules basically shows the associations of features of the items on promotions. The association rules should show what are the features associated with high lift or profit.  
The association rules should be done on each category of items (i.e. for example on all types of yogurts). The rules basically shows the associations of features of the items on promotions. The association rules should show what are the features associated with high lift or profit.  
You may need to convert the numbers into ranges. Depending on the data the ranges can be up to 10 chunks we can discuss this if you need.
If you don't know how the a-priori works and what the output looks like have a look at this example  [Association Rules R]. 
If the data size is big you may need to use the Scala implementation of the [Association Rules Spark]
---------------------------------------
### Step3: Predicting optimal promotion criteria for a particular item (when to promote, length of promotion in weeks, optimal price reduction)

Basically we need you to build a regression model to predict the number of items sold based on the type of promotion, length of promotion, and the percentage of price reduction. Use the dataset you have built in phase 1 to do this task. 

# Useful links:
[Azure Example] https://gallery.cortanaintelligence.com/Experiment/HARMAN-ANALYTICS-Promotion-Effectiveness-1 
[Association Rules R] https://www.r-bloggers.com/implementing-apriori-algorithm-in-r/ 
[Association Rules Spark] https://spark.apache.org/docs/2.0.2/mllib-frequent-pattern-mining.html