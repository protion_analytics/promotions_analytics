#!/usr/bin/python
from collections import OrderedDict

import pandas as pd
# import os
import psycopg2
# import mysql.connector
from sqlalchemy import create_engine, types  # , MetaData
import hashlib
import ConfigParser

import promo_constants
import pa_enrich_data as pa


# sample usage:
# pr = PromoAnalyzer()
# pr.perform_etl('41351','37000')

class PromoAnalyzer:
    def __init__(self):
        # self.file_path= file_path
        # Todo: secure this more
        # Todo: test the code snippet below:
        # ====================================
        config = ConfigParser.RawConfigParser()
        config.read('conf.properties')

        section_name = 'DatabaseSection'
        self.__HOST = config.get(section_name, 'database.host')
        self.__PORT = config.get(section_name, 'database.port')
        self.__USER = config.get(section_name, 'database.user')
        self.__PASSWORD = config.get(section_name, 'database.password')
        self.__DATABASE = config.get(section_name, 'database.databasename')
        self.__SCHEMA = config.get(section_name, 'database.schema')

        section_name = 'ExternalData'
        self.__WEATHER_FILE = config.get(section_name, 'data.weather_file_path')
        self.__EVENT_FILE = config.get(section_name, 'data.event_file_path')
        self.__GDP_BY_STATE_FILE = config.get(section_name, 'data.gdp_by_state')
        self.__EMPLOYMENT_RATE_BY_STATE_FILE = config.get(section_name, 'data.employment_by_state')
        self.__HOUSING_PRICES_BY_STATE_FILE = config.get(section_name, 'data.housing_prices_by_state')

        # ====================================
        print("Initialized a PromoAnalyzer instance")

    def generateMD5Hash(self, inputString):
        # Assumes the default UTF-8
        return hashlib.md5(inputString.replace(" ", "").encode()).hexdigest()

    def redshift_db_connection(self):
        conn = psycopg2.connect(
            host=self.__HOST,
            port=self.__PORT,
            user=self.__USER,
            password=self.__PASSWORD,
            database=self.__DATABASE,
        )
        print("Established connection to Redshift")
        return conn

    def fetch_store_data(self, item=None, vend=None):
        dataset_item = item
        dataset_vend = vend

        optionalWhereClause = ''
        if dataset_item != None:
            optionalWhereClause += 'item = ' + dataset_item + ' '
        if dataset_vend != None:
            if len(optionalWhereClause) > 0:
                optionalWhereClause += 'AND '

            optionalWhereClause += 'vend = ' + dataset_vend + ' '

        if len(optionalWhereClause) > 0:
            # we have either item or vend or both specified
            optionalWhereClause = 'WHERE ' + optionalWhereClause  # + ' LIMIT 100;'
        else:
            # we dont have either item or vend specified
            optionalWhereClause = ';'

        return self._fetch_table_data(promo_constants.STORE_DATA_TABLE_COLUMNS, promo_constants.STORE_DATA_TABLE_NAME,
                                      optionalWhereClause)

    def _fetch_frame_for_query(self, sql_query):

        df = pd.DataFrame()
        conn = self.redshift_db_connection()

        try:
            print("Running Query on Redshift: \n {0}".format(sql_query))
            df = pd.read_sql(sql_query, conn)
            # df.to_csv('tmp/queryresult.csv', index=False)
        finally:
            print("Closing connection to Redshift")
            conn.close()

        return df

    def _fetch_table_data(self, columns, table_name, optional_clause=None):
        select_query = 'SELECT {0} FROM {1}'.format(
            ', '.join(columns), table_name)

        if optional_clause is not None:
            select_query += ' ' + optional_clause

        return self._fetch_frame_for_query(select_query)

    def perform_etl(self, item=None, vend=None):

        df = self.fetch_store_data(item, vend)
        df_demo = self._fetch_table_data(promo_constants.STORE_DEMOGRAPHICS_TABLE_COLUMNS,
                                         promo_constants.STORE_DEMOGRAPHICS_TABLE_NAME)
        df_product_attr = self._fetch_table_data(promo_constants.PRODUCT_ATTRIBUTES_TABLE_COLUMNS,
                                                 promo_constants.PRODUCT_ATTRIBUTES_TABLE_NAME)

        # DEV csv
        # df = pd.read_csv('data/store_data_{0}_{1}.csv'.format(item, vend))[:100]
        # df_demo = pd.read_csv('data/demo_data.csv')
        # df_product_attr = pd.read_csv('data/product_attributes_y12.csv')

        df_weather = pd.read_csv(self.__WEATHER_FILE)
        df_event = pd.read_csv(self.__EVENT_FILE)
        df_gdp_all_states = pd.read_csv(self.__GDP_BY_STATE_FILE)
        df_employment_rate_all_states = pd.read_csv(self.__EMPLOYMENT_RATE_BY_STATE_FILE)
        df_housing_prices_all_states = pd.read_csv(self.__HOUSING_PRICES_BY_STATE_FILE)

        for year in df['year'].unique():
            self._perform_etl_for_year(year, df[df['year'] == year], df_demo, df_product_attr, df_weather,
                                       df_event, df_gdp_all_states, df_employment_rate_all_states,
                                       df_housing_prices_all_states,
                                       item, vend)

    def _perform_etl_for_year(self, data_year, df, df_demo, df_product_attr, df_weather,
                              df_event, df_gdp, df_employment_rate,
                              df_housing_prices, item=None, vend=None):

        first_week = promo_constants.START_WEEK_BY_YEAR[data_year]

        if len(df.index) <= 0:
            print(
                "No rows returned for the Redshift query using the specified (item,vend) pair in year-" + str(
                    data_year))
            return False

        df = df.sort_values(['item', 'vend', 'store_id', 'week'], ascending=[True, True, True, True])

        # sweep through dataframe and
        # df2 = df[df['store_id']=='200156']
        # print(df2)
        # total_dollars = df.groupby(['store_id'])['dollars'].count()

        # create a new empty dataframe to hold the promo table

        df_promo = pd.DataFrame(columns=promo_constants.PROMO_TABLE_COLUMNS)
        df_non_promo = pd.DataFrame(columns=promo_constants.NON_PROMO_TABLE_COLUMNS)

        # use 5 weeks prior for window length
        window_pre = 5  # in weeks
        window_post = 5  # in weeks

        cur_total_non_promo_units_sold = 0
        cur_total_non_promo_dollar_sales = 0
        cur_total_non_promo_weeks = 0
        cur_total_non_promo_data_points = 0
        cur_non_promo_avg_price_point = 0

        last_store_id = -1
        last_item = -1
        last_vend = -1
        promo_start_week = -1
        last_year = -1

        total_rows = len(df.index)

        nonPromoSample = 0
        print("Beginning promo analysis for year {0}; will analyze a total of {1} rows".format(data_year,
                                                                                               str(total_rows)))
        # ----------------------------------------------------------------------
        progress_range = range(len(df))
        iter_count = 0

        for index, row in df.iterrows():

            cur_pr = int(row['pr'])
            cur_week = int(row['week'])
            cur = int(row['store_id'])
            cur_item = int(row['item'])
            cur_vend = int(row['vend'])
            cur_units = int(row['units'])
            cur_year = int(row['year'])
            cur_upc = self._get_upc(row)

            pa.print_progress(progress_range, iter_count)
            iter_count += 1

            if last_store_id != cur:
                # bar.next()
                # we have moved on to the next store_id
                # rest variables
                # print 'Analyzing group with store_id = ', cur

                if last_store_id != -1:
                    # update the non-promo week table
                    avg_weekly_non_promo_units_sold = 0
                    avg_non_promo_unit_price = 0

                    if cur_total_non_promo_weeks != 0:
                        avg_weekly_non_promo_units_sold = round(
                            cur_total_non_promo_units_sold / (cur_total_non_promo_weeks * 1.0), 2)
                    if cur_total_non_promo_data_points != 0:
                        avg_non_promo_unit_price = round(
                            cur_non_promo_avg_price_point / (cur_total_non_promo_data_points * 1.0), 2)
                    ## print 'Updating non-promo table'
                    entryIndex = len(df_non_promo)
                    df_non_promo.loc[entryIndex] = [entryIndex - 1, last_item, last_vend, last_store_id, cur_year,
                                                    cur_upc,
                                                    avg_weekly_non_promo_units_sold, avg_non_promo_unit_price, '']
                    # as a final step add the md5hash column to uniquely identify this row in the table based on its values
                    md5hashStr = self.generateMD5Hash(str(df_non_promo.ix[entryIndex].values))
                    df_non_promo.set_value(entryIndex, 'md5hash',
                                           md5hashStr)  # use entryIndex since the length of the length of the data frame has increaased by one to the prior insert

                prior_week_pr = 0
                cur_total_non_promo_units_sold = 0
                cur_total_non_promo_dollar_sales = 0
                cur_total_non_promo_weeks = 0
                cur_total_non_promo_data_points = 0
                cur_non_promo_avg_price_point = 0

                cur_total_promo_units_sold = 0
                cur_total_promo_dollar_sales = 0
                cur_total_promo_weeks = 0
                cur_total_promo_data_points = 0
                cur_promo_avg_price_point = 0
                cur_promo_features = []
                cur_promo_display = []

                # pre/post promo windows
                window_units = []
                window_sales = []
                pre_promo_units_window = []
                pre_promo_sales_window = []
                pre_promo_window_length = 0
                total_sales_pre_promo = 0
                total_units_pre_promo = 0
                prior_week = first_week - 1  # assume all weeks not present in current year are missing

            # update store_id
            last_store_id = cur

            last_item = cur_item
            last_vend = cur_vend
            last_year = cur_year
            last_upc = cur_upc

            # we are still analyzing the same store_id
            missing_samples = cur_week - prior_week - 1

            # update week
            prior_week = cur_week

            if cur_pr == 0:
                # a non-promo day

                # only update the window in non-promo weeks
                window_units = window_units + [0] * missing_samples
                window_units.append(cur_units)

                window_sales = window_sales + [0] * missing_samples
                window_sales.append(row['dollars'])

                cur_total_non_promo_units_sold += cur_units
                cur_total_non_promo_dollar_sales += row['dollars']
                if cur_units != 0:
                    cur_non_promo_avg_price_point += round((row['dollars'] / (cur_units * 1.0)), 2)

                cur_total_non_promo_weeks += (1 + missing_samples)  # accounts for missing weeks
                cur_total_non_promo_data_points += 1  # ignores missing weeks

                if prior_week_pr == 1:
                    # promo -> non_promo; this signals the end of a past promotion
                    ##print('End of Promo; Adding entry to promo table')
                    # write an entry into the final table for this promotion
                    # note: -1 or 0 or -99999 entries will be updated at a subsequent time
                    avg_promo_unit_price = 0

                    if cur_total_promo_data_points != 0:
                        avg_promo_unit_price = round((cur_promo_avg_price_point / (cur_total_promo_data_points * 1.0)),
                                                     2)

                    df_promo.loc[len(df_promo)] = [len(df_promo) - 1,
                                                   cur_item,
                                                   cur_vend,
                                                   cur,
                                                   cur_year,
                                                   cur_upc,
                                                   promo_start_week,
                                                   str(list(OrderedDict.fromkeys(cur_promo_features))),
                                                   # TODO: find a better way to store this
                                                   str(list(OrderedDict.fromkeys(cur_promo_display))),
                                                   # TODO: find a better way to store this
                                                   avg_promo_unit_price,
                                                   cur_total_promo_weeks,
                                                   cur_total_promo_data_points,
                                                   cur_total_promo_units_sold,
                                                   cur_total_promo_dollar_sales,
                                                   pre_promo_window_length,
                                                   0,
                                                   total_units_pre_promo,
                                                   total_sales_pre_promo,
                                                   0,
                                                   0,
                                                   -99999,  # this is a 'NaN' like value indicating an invalid entry
                                                   -99999,  # this is a 'NaN' like value indicating an invalid entry
                                                   '']

                    # reset the length of the prepromo window
                    pre_promo_window_length = 0
            else:
                # a promo week
                if prior_week_pr == 0:
                    # non-promo -> promo transition (reset all promo related parameters)
                    ##print('Start of Promo')
                    cur_total_promo_units_sold = 0
                    cur_total_promo_dollar_sales = 0
                    cur_total_promo_weeks = 0
                    cur_total_promo_data_points = 0
                    cur_promo_avg_price_point = 0
                    cur_promo_features = []
                    cur_promo_display = []

                    promo_start_week = cur_week
                    window_units = window_units + [0] * missing_samples
                    window_sales = window_sales + [0] * missing_samples
                    # get the last N weeks of the pre-promo window
                    pre_promo_units_window = window_units[-window_pre:]
                    pre_promo_sales_window = window_sales[-window_pre:]
                    pre_promo_window_length = len(pre_promo_units_window)

                    total_units_pre_promo = sum(pre_promo_units_window)
                    total_sales_pre_promo = sum(pre_promo_sales_window)

                    # this post promo corresponds to the prior promotion
                    # get the first N weeks of the post-promo window
                    post_promo_units_window = window_units[:window_post]
                    post_promo_sales_window = window_sales[:window_post]

                    total_units_post_promo = sum(post_promo_units_window)
                    total_sales_post_promo = sum(post_promo_sales_window)

                    # update the last promo entry corresponding to the current key (if any) with the post promo values
                    if len(df_promo) > 0:
                        # there is at least one entry in the promo table so far
                        last_promo_indx = len(df_promo) - 1
                        if df_promo.loc[last_promo_indx]['store_id'] == cur:
                            # this means there is a past promo in the table corresponding to this store_id
                            # we need to update its post promo info here
                            ##print 'Backtracking - updating post-promo window of past promo'

                            df_promo.set_value(last_promo_indx, 'number_of_weeks_in_post_promo_window',
                                               len(post_promo_units_window))
                            df_promo.set_value(last_promo_indx, 'total_units_sold_in_post_promo_window',
                                               total_units_post_promo)
                            df_promo.set_value(last_promo_indx, 'total_dollar_sales_in_post_promo_window',
                                               total_sales_post_promo)

                    # reset the pre/post promo windows
                    window_units = []
                    window_sales = []
                    pre_promo_units_window = []
                    pre_promo_sales_window = []
                    post_promo_units_window = []
                    post_promo_sales_window = []

                cur_total_promo_units_sold += cur_units
                cur_total_promo_dollar_sales += row['dollars']
                cur_promo_features.append(row['f'])
                cur_promo_display.append(row['d'])
                if cur_units != 0:
                    cur_promo_avg_price_point += round((row['dollars'] / (cur_units * 1.0)), 2)
                if prior_week_pr == 1:
                    cur_total_promo_weeks += (
                        1 + missing_samples)  # accounts for missing weeks as if they wer promo weeks 1?1 -> ?=1
                else:
                    cur_total_promo_weeks += 1  # since we assume 0?1 then we assume ? = 0 (i.e. a non promo week)
                    cur_total_non_promo_weeks += missing_samples  # accounts for missing weeks

                cur_total_promo_data_points += 1  # ignores missing weeks

            # update the prior week pr vlaue
            prior_week_pr = row['pr']
        # ----------------------------------------------------------------------
        # update the non-promo week table (for the last entry)
        avg_weekly_non_promo_units_sold = -1
        avg_non_promo_unit_price = -1

        if cur_total_non_promo_weeks != 0:
            avg_weekly_non_promo_units_sold = round(cur_total_non_promo_units_sold / (cur_total_non_promo_weeks * 1.0),
                                                    2)
        if cur_total_non_promo_data_points != 0:
            avg_non_promo_unit_price = round(cur_non_promo_avg_price_point / (cur_total_non_promo_data_points * 1.0), 2)

        entryIndex = len(df_non_promo)
        df_non_promo.loc[entryIndex] = [entryIndex - 1, last_item, last_vend, last_store_id, last_year,
                                        last_upc, avg_weekly_non_promo_units_sold, avg_non_promo_unit_price, '']
        # as a final step add the md5hash column to uniquely identify this row in the table based on its values
        md5hashStr = self.generateMD5Hash(str(df_non_promo.ix[entryIndex].values))
        df_non_promo.set_value(entryIndex, 'md5hash',
                               md5hashStr)  # use entryIndex since the length of the length of the data frame has increaased by one to the prior insert

        # bar.finish()

        # now for each promo entry compute the price reduction by comparing the avg price on non-promo days to the avg price on promo days

        print("Computing promotion metrics (lift & profit)")
        # the promo's profit is the total money made during the promo minus the los due to the price rduction
        # profit_during_promo_period = total_sales_in_promo_period - (number_of_promo_weeks * (NON_promo_price - promo_price) )
        # lift1 = total_sales_during_promo/total_prom_weeks - total_sales_during_non_promo_pre/post_window/non_promo_pre/post_window
        # if lift1 is big => avg money made during promotional period brought in an increase in profit as opposed to avg money made on non promo day => successful promo!
        # lift2 = total_sales_post_promo_window  - total_sales_pre_window
        progress_range = range(len(df))
        iter_count = 0

        for index, row in df_promo.iterrows():

            pa.print_progress(progress_range, iter_count)
            iter_count += 1

            profit = 0
            avg_non_promo_unit_price = df_non_promo[df_non_promo['store_id'] == row['store_id']][
                'avg_weekly_price_point_non_promo_weeks']

            if avg_non_promo_unit_price.empty == False:
                profit = row['total_dollar_sales_value_in_promo'] - row['number_of_promo_weeks'] * (
                    avg_non_promo_unit_price - row['avg_promo_unit_price'])
            df_promo.set_value(index, 'promo_profit', profit)
            lift = -1
            number_of_non_promo_weeks = row['number_of_weeks_in_pre_promo_window'] + row[
                'number_of_weeks_in_post_promo_window']
            if row['number_of_promo_weeks'] != 0 and number_of_non_promo_weeks != 0:
                lift = row['total_dollar_sales_value_in_promo'] / (1.0 * row['number_of_promo_weeks']) - (row[
                                                                                                              'total_units_sold_in_pre_promo_window'] +
                                                                                                          row[
                                                                                                              'total_units_sold_in_post_promo_window']) / (
                                                                                                             1.0 * number_of_non_promo_weeks)
            df_promo.set_value(index, 'promo_lift', lift)

            # as a final step add the md5hash column to uniquely identify this row in the table based on its values
            md5hashStr = self.generateMD5Hash(str(df_promo.ix[index].values))
            df_promo.set_value(index, 'md5hash', md5hashStr)

        # Enriching data with weather, events and applying FFT
        print('Enriching data with weather, events and applying FFT')

        df_promo = pa.merge_tables(df_demo, df_product_attr, df_promo)
        df_non_promo = pa.merge_tables(df_demo, df_product_attr, df_non_promo)

        pa.enrich_with_weather_data(target_df=df_promo,
                                    weather_df=df_weather, is_promo=True)

        # TODO clarify how we deal with non promo
        # pa.enrich_with_weather_data(target_df=df_non_promo,
        #                               weather_df=df_weather, is_promo=False)
        # df_non_promo = pa.enrich_with_event_data(target_df=df_non_promo, event_df=df_event, is_promo=False)

        pa.enrich_with_event_data(target_df=df_promo, event_df=df_event, is_promo=True)
        pa.enrich_with_economic_data(df_promo, df_gdp, df_employment_rate, df_housing_prices)
        pa.enrich_with_economic_data(df_non_promo, df_gdp, df_employment_rate, df_housing_prices)

        df_promo = pa.apply_fft(df_promo)

        print("Promo Analysis complete")
        print '-------------------------------'

        print("Number of promotions discovered: " + str(len(df_promo.index)))
        print("Number of store_ids proccessed: " + str(len(df_non_promo.index)))
        print '-------------------------------'
        # save the resulting datframes to a tsv
        # df_promo.to_csv(dirname +'/'+ filename +'_promo_table.tsv',sep='\t', header=True, index=False)
        # df_non_promo.to_csv(dirname +'/'+ filename +'_non_promo_table.tsv',sep='\t', header=True, index=False)

        # TODO: dont overwrite tables; try to append to them or dont do anything

        filename = 'year' + str(data_year)
        if item != None:
            filename += '_item' + str(item)

        if vend != None:
            filename += '_vend' + str(vend)

        # df_promo.to_csv('result/promo_{0}.csv'.format(filename), index=False)
        # df_non_promo.to_csv('result/non_promo_{0}.csv'.format(filename), index=False)

        self.save2Db(df=df_promo, tableName='promo_' + filename,
                     columnTypes=promo_constants.EVENT_COLUMN_DICT)
        self.save2Db(df=df_non_promo, tableName='non_promo_' + filename,
                     columnTypes=None)

        return True

    def _chunks(self, l, n):
        n = max(1, n)
        return [l[i:i + n] for i in range(0, len(l), n)]

    # TODO: make this a more generic interface with ability to save to other databss than mysql (like C*, Redshift, etc..)
    def save2Db(self, df, tableName, columnTypes):
        host = self.__HOST
        port = self.__PORT
        user = self.__USER
        password = self.__PASSWORD
        database = self.__DATABASE
        denoted_schema = self.__SCHEMA
        engine_string = "postgresql+psycopg2://%s:%s@%s:%s/%s" \
                        % (user, password, host, port, database)
        engine = create_engine(engine_string)
        conn = engine.connect()
        # metadata = MetaData(conn)
        # test = pd.read_sql_query(test_query,red_engine)
        try:
            print("Writing dataframe <" + tableName + "> to Redshift")
            # df.to_sql('data_chunked', engine, chunksize=1000)
            chunkSize = 100
            # creatTableSqlPromo = "create table if not exists promo_year12_item41351_vend37000 \
            chunkArray = self._chunks(range(0, len(df.index) - 1), chunkSize)
            k = 0
            for i in chunkArray:
                k += 1
                df.ix[i].to_sql(name=tableName, con=conn, schema=denoted_schema, if_exists='append', index=False,
                                chunksize=100,
                                dtype=columnTypes)
                print("writing chunk #" + str(k) + "/" + str(len(chunkArray)) + " to Redshift")

                # df.to_csv('tmp/queryresult.csv', index=False)

        except Exception as e:
            print str(e)
        finally:
            print("Closing connection to Redshift")
            conn.close()

    @staticmethod
    def _get_upc(row):
        return '{0}-{1}-{2}-{3}'.format(str(int(row['sy'])).zfill(2), str(int(row['ge'])).zfill(2),
                                        str(int(row['vend'])).zfill(5), str(int(row['item'])).zfill(5))
