import eventful
import threading
import pandas as pd
import numpy as np
import datetime
import httplib2
import StringIO
import urllib
import quandl
import promo_constants
import pa_enrich_data as pa


class EventDataImporter:
    """
    Event data importer.
    Invokes eventful rest API and retrieves event data using specified criterion.
    You can specify 'date' parameter to fetch some specific date interval or use the default one:

    importer = ubc_import_data.EventDataImporter('_eventful_api_key')

    importer.import_events_chunk([list_of_zip_codes], 'events_24.12.16:22:32.csv')
    or
    importer.import_events_chunk([list_of_zip_codes],
            'events_24.12.16:22:32.csv', chunk_size=350 date_interval='2011010100-2013010100')

    """

    def __init__(self, api_key):
        self.api_key = api_key

    def import_events_chunk(self, zip_codes, output_file_name, date_interval='2011010100-2013010100', chunk_size=350):
        """
        Retrieve event data by specific chunks using multi-threading

        :date_interval date value of format described here: http://api.eventful.com/docs/events/search
        """
        zip_len = len(zip_codes)
        frame_all_events = pd.DataFrame()

        for index, chunk in enumerate(list(self._chunks(zip_codes, chunk_size))):
            print '{0}/{1}'.format(index * chunk_size, zip_len)
            frame_all_events = self._import_events(chunk, date_interval, frame_all_events)

        frame_all_events.to_csv(output_file_name, index=False, encoding='utf-8')

    @staticmethod
    def _chunks(l, n):
        for i in range(0, len(l), n):
            yield l[i:i + n]

    def _import_events_for_zip(self, data_list, zip_code, date_interval):
        eventful_api = eventful.API(self.api_key)

        try:
            res = eventful_api.call('/events/search', location=zip_code, page_size=250, date=date_interval)
            data_list.append(res)

            for page_number in range(1, int(res['page_count'])):
                data_list.append(eventful_api.call('/events/search', location=zip_code, page_size=250,
                                                   date=date_interval, page_number=page_number))
        except:
            print 'Repeat for zip: {0}'.format(zip_code)
            self._import_events_for_zip(data_list, zip_code, date_interval)

    def _import_events(self, zip_codes, date_interval, frame_all_events):
        all_event_data = []
        threads = []

        for zip_code in zip_codes:
            thread = threading.Thread(target=self._import_events_for_zip,
                                      args=(all_event_data, zip_code, date_interval))
            threads.append(thread)
            thread.start()

        for thread in threads:
            thread.join()

        for event in all_event_data:
            if int(event['total_items']) > 0:
                event_entry = event['events']['event']
                if int(event['total_items']) == 1:
                    frame_all_events = frame_all_events.append(pd.DataFrame(event_entry, index=[0]))
                else:
                    frame_all_events = frame_all_events.append(pd.DataFrame(event_entry))

        return frame_all_events


class WeatherCriterionExporter:
    """
    Helper class to extract the weather criterion for weather-api.
    """

    def __init__(self):
        pass

    @staticmethod
    def _year_day_to_date(year, day):
        return datetime.datetime(year, 1, 1) + datetime.timedelta(day - 1)

    def export_weather_criteria(self, df_store_data, df_demo, output_file_name):
        """
        :param df_store_data: store_data table
        :param df_demo: store_demographics table
        :param output_file_name: output file name
        :return: None
        """
        df_merged = pd.merge(df_store_data, df_demo, on='store_id')
        df_weather_criteria = pd.DataFrame(columns=['uniqid', 'zip', 'year', 'month', 'day'])

        distinct_zip_count = len(df_merged['zip_code'].unique())
        distinct_year_count = len(df_merged['year'].unique())

        days_per_year = 365
        export_criteria_length = distinct_year_count * distinct_zip_count * days_per_year

        days = np.array(
            [range(1, days_per_year * distinct_year_count + 1) for zip_entry in range(distinct_zip_count)]).reshape(
            export_criteria_length)
        years = np.array(
            [np.repeat(year, distinct_zip_count * days_per_year) for year in df_merged['year'].unique()]).reshape(
            export_criteria_length)
        zip_codes = np.array(
            [np.repeat(zip_code_entry, days_per_year * distinct_year_count) for zip_code_entry in
             df_merged['zip_code'].unique()]).reshape(
            export_criteria_length)

        df_weather_criteria['uniqid'] = range(1, export_criteria_length + 1)
        df_weather_criteria['zip'] = zip_codes
        df_weather_criteria['day'] = days
        df_weather_criteria['year'] = years
        df_weather_criteria = df_weather_criteria.assign(month=
                                                         lambda x: [self._year_day_to_date(year, day).month for
                                                                    year, day
                                                                    in zip(list(x.year), list(x.day))])
        df_weather_criteria = df_weather_criteria.assign(day=
                                                         lambda x: [self._year_day_to_date(year, day).day for year, day
                                                                    in zip(list(x.year), list(days))])

        return df_weather_criteria.to_csv(output_file_name, index=False)


class QuandlDataExporter:
    """
    Helper class to retrieve data sets from Quandl

    Usage: QuandlDataExporter('quandl_key')
    """

    def __init__(self, quandl_key):
        self.http = httplib2.Http()
        quandl.ApiConfig.api_key = quandl_key

    def run_quandl_query(self, url_pattern, **search_params):
        """
        Usage:
        run_quandl_query('https://www.quandl.com/api/v3/datasets.csv', query='GDP CA', database_code='FRED')

        """
        args = urllib.urlencode(search_params)
        url = url_pattern + '?{0}'.format(args)

        response, content = self.http.request(url, 'GET')

        status = int(response['status'])
        if status == 200:
            return pd.read_csv(StringIO.StringIO(content))
        else:
            print 'request failed with status: {0}'.format(status)

    def get_gdp_empl_housing_fred(self):
        """
        Wrapper function to retrieve all possible data sets from Quandl/FRED
        :return: gdp, employment rate and housing prices for all US states
        """
        # Since Quandl has text based search only in some specific cases it can return wrong data sets
        # TODO tweak it filtering data set codes description

        gdp_data_sets = self.find_data_sets_for_state(promo_constants.STATE_ABBR_MAP.keys(), 'GDP')
        employment_data_sets = self.find_data_sets_for_state(promo_constants.STATE_ABBR_MAP.keys(), 'Employment rate')
        housing_data_sets = self.find_data_sets_for_state(promo_constants.STATE_ABBR_MAP.keys(), 'Housing prices')

        print 'Searching for States GDP'
        gdp = self.extract_data_sets_for_states(gdp_data_sets)
        print 'Searching for States employment rates'
        employment = self.extract_data_sets_for_states(employment_data_sets)
        print 'Searching for States housing prices index'
        housing = self.extract_data_sets_for_states(housing_data_sets)

        return gdp, employment, housing

    # TODO results for counties are too broad since Quandl search is only text based
    def find_gdp_county_data_sets(self, df_demo):
        df_demo['state_name_abbr'] = df_demo.state_name.map(lambda state: promo_constants.STATE_ABBR_MAP[state])
        df_data_set = pd.DataFrame()

        for index, row in df_demo.iterrows():
            df_data_set = df_data_set.append(
                self.run_quandl_query('https://www.quandl.com/api/v3/datasets.csv',
                                      query='GDP {0}, {1}'.format(row.county_name, row.state_name_abbr),
                                      database_code='FRED', page=1, per_page=1))

        return df_data_set

    def find_data_sets_for_state(self, state_names, query_str):
        """
        :param state_names: array with state names
        :param query_str: like 'GDP' or 'Housing prices'
        :return:
        """
        df_data_set = pd.DataFrame()
        progress_range = range(len(state_names))

        for index, state_name in enumerate(state_names):
            pa.print_progress(progress_range, index)
            res = self.run_quandl_query('https://www.quandl.com/api/v3/datasets.csv',
                                        query='{1} {0}'.format(state_name, query_str),
                                        database_code='FRED', page=1, per_page=1)
            if res is not None:
                df_data_set = df_data_set.append(res.assign(state_name=state_name))

        return df_data_set

    def extract_data_sets_for_states(self, data_codes_df):
        """
        :param data_codes_df: data codes with state_name attribute
        :return: DataFrame with data sets
        """
        data_sets = []
        progress_range = range(len(data_codes_df))

        for index, row in data_codes_df.iterrows():
            pa.print_progress(progress_range, index)
            data_set = quandl.get('FRED/{0}'.format(row.dataset_code))
            if data_set is not None:
                data_sets.append(data_set.rename(columns={'VALUE': row.state_name}))

        res = pd.concat(data_sets, axis=1)
        return res
