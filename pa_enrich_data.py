import pandas as pd
import numpy as np
import datetime
import re


def enrich_with_event_data(target_df, event_df, is_promo):
    if is_promo:
        print 'Enrichment with events data for promo sample'
    else:
        print 'Enrichment with events data for non promo sample'

    range_index = range(len(target_df))

    for index, row in target_df.iterrows():
        events_per_zip = event_df[event_df['postal_code'] == row['zip_code']]

        print_progress(range_index, index)
        if len(events_per_zip) == 0:
            continue

        if is_promo:
            date_promo_start, date_promo_end = _get_date_interval_from_promo_row(row)

            ev_title = []
            ev_start_time = []
            ev_venue_name = []
            ev_venue_address = []
            ev_city_name = []
            ev_region_name = []
            ev_postal_code = []
            ev_country_name = []
            ev_all_day = []
            ev_trackback_count = []  # missing in the event response from eventful api
            ev_calendar_count = []
            ev_comment_count = []
            ev_link_count = []
            ev_created = []
            ev_owner = []

            for index_event, event in events_per_zip.iterrows():
                if _is_within_interval(date_promo_start, date_promo_end, event['start_time'], event['stop_time']):
                    ev_title.append(_extract_event_prop(event, 'title'))
                    ev_start_time.append(_extract_event_prop(event, 'start_time'))
                    ev_venue_name.append(_extract_event_prop(event, 'venue_name'))
                    ev_venue_address.append(_extract_event_prop(event, 'venue_address'))
                    ev_city_name.append(_extract_event_prop(event, 'city_name'))
                    ev_region_name.append(_extract_event_prop(event, 'region_name'))
                    ev_postal_code.append(_extract_event_prop(event, 'postal_code'))
                    ev_country_name.append(_extract_event_prop(event, 'country_name'))
                    ev_all_day.append(_extract_event_prop(event, 'all_day'))
                    ev_calendar_count.append(_extract_event_prop(event, 'calendar_count'))
                    ev_comment_count.append(_extract_event_prop(event, 'comment_count'))
                    ev_link_count.append(_extract_event_prop(event, 'link_count'))
                    ev_created.append(_extract_event_prop(event, 'created'))
                    ev_owner.append(_extract_event_prop(event, 'owner'))

            target_df.set_value(index, 'ev_title', _event_list_to_string(ev_title))
            target_df.set_value(index, 'ev_start_time', _event_list_to_string(ev_start_time))
            target_df.set_value(index, 'ev_venue_name', _event_list_to_string(ev_venue_name))
            target_df.set_value(index, 'ev_venue_address', _event_list_to_string(ev_venue_address))
            target_df.set_value(index, 'ev_city_name', _event_list_to_string(ev_city_name))
            target_df.set_value(index, 'ev_region_name', _event_list_to_string(ev_region_name))
            target_df.set_value(index, 'ev_postal_code', _event_list_to_string(ev_postal_code))
            target_df.set_value(index, 'ev_country_name', _event_list_to_string(ev_country_name))
            target_df.set_value(index, 'ev_all_day', _event_list_to_string(ev_all_day))
            target_df.set_value(index, 'ev_calendar_count', _event_list_to_string(ev_calendar_count))
            target_df.set_value(index, 'ev_comment_count', _event_list_to_string(ev_comment_count))
            target_df.set_value(index, 'ev_link_count', _event_list_to_string(ev_link_count))
            target_df.set_value(index, 'ev_created', _event_list_to_string(ev_created))
            target_df.set_value(index, 'ev_owner', _event_list_to_string(ev_owner))

    return None


def _event_list_to_string(ev_list):
    if len(ev_list) == 0:
        return None
    else:
        return str(ev_list)


def _extract_event_prop(event, prop_name):
    val = event[prop_name]

    if prop_name == 'venue_address':
        if type(val) == str:
            parts = re.findall('^[0-9]+', val)
            if len(parts) > 0:
                val = val.split(parts[0])[1]

    if prop_name == 'created':
        val = _date_str_to_day_period(val)

    return val


def _date_str_to_day_period(date_str):
    date_val = _eventful_str_to_date(date_str)
    val = date_str

    if date_val is not None:
        hour = date_val.hour

        if hour < 4 or hour >= 20:
            val = 'Night'
        elif 4 <= hour < 12:
            val = 'Morning'
        elif 12 <= hour < 16:
            val = 'Afternoon'
        elif 16 <= hour < 20:
            val = 'Evening'

    return val


def _is_within_interval(promo_start, promo_finish, ev_start, ev_finish):
    ev_start_time = None
    ev_finish_time = None

    try:
        ev_start_time = _eventful_str_to_date(ev_start)
        ev_finish_time = _eventful_str_to_date(ev_finish)
    except:
        pass

    return (ev_start_time is not None and promo_start > ev_start_time) and (
        ev_finish_time is not None and promo_finish < ev_finish_time)


def _eventful_str_to_date(date_str):
    return datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")


def enrich_with_weather_data(target_df, weather_df, is_promo):
    if is_promo:
        print 'Enriching with weather data for promo sample'
    else:
        print 'Enriching with weather data for non promo sample'

    weather_df = weather_df.assign(
        time=lambda x: [str(datetime.datetime(year=year, month=month, day=day)) for year, month, day in zip(
            list(x.year), list(x.month), list(x.day))])
    weather_df['time'] = pd.DatetimeIndex(weather_df['time'])
    weather_df.set_index(keys='time', inplace=True)
    weather_df['time'] = weather_df.index

    range_index = range(len(target_df))

    for index, row in target_df.iterrows():

        # calculate weather metrics regarding promo week time interval
        if is_promo:
            promo_year = str(int(row['year']))
            promo_week_start = int(row['promo_start_week'])
            number_of_promo_weeks = int(row['number_of_promo_weeks'])
            promo_week_end = promo_week_start + number_of_promo_weeks
            date_promo_start = _week_to_date(promo_year, promo_week_start)
            date_promo_end = _week_to_date(promo_year, promo_week_end)

            promo_time_interval = weather_df[
                ((weather_df['time'] > date_promo_start) & (weather_df['time'] < date_promo_end))]
        else:
            # calculate weather metrics despite of promo week for non promotion sample
            promo_time_interval = weather_df

        promo_time_interval = promo_time_interval[promo_time_interval['zip'] == row['zip_code']]
        average_temp_vector = (promo_time_interval.TMIN + promo_time_interval.TMAX) / 2
        average_temp_vector *= 0.1  # convert from tenth to normal Celsius degree

        target_df.set_value(index, 'avg_t', average_temp_vector.mean())
        target_df.set_value(index, 'std_t', average_temp_vector.std())
        target_df.set_value(index, 'min_t', average_temp_vector.min())
        target_df.set_value(index, 'max_t', average_temp_vector.max())

        print_progress(range_index, index)

    return None


def _week_to_date(year, week):
    d = '{0}-W{1}'.format(year, week)
    result = datetime.datetime.strptime(d + '-0', "%Y-W%W-%w")
    return result


def _ev_string_to_date(str_date):
    result = datetime.datetime.strptime(str_date, "%Y-m-d%")
    return result


def _date_to_eventful_format(date):
    return date.strftime('%Y%m%d00')


def _get_date_interval_from_promo_row(row):
    promo_year = str(int(row['year']))
    promo_week_start = int(row['promo_start_week'])
    number_of_promo_weeks = int(row['number_of_promo_weeks'])
    promo_week_end = promo_week_start + number_of_promo_weeks
    date_promo_start = _week_to_date(promo_year, promo_week_start)
    date_promo_end = _week_to_date(promo_year, promo_week_end)

    return date_promo_start, date_promo_end


def print_progress(range_index, index):
    if index == len(range_index) - 1:
        print '100% complete'

    elif index == int(np.percentile(range_index, 75)):
        print '75% complete'

    elif index == int(np.percentile(range_index, 50)):
        print '50% complete'

    elif index == int(np.percentile(range_index, 25)):
        print '25% complete'


def merge_tables(df_store_demo, df_product_attr, df_target):
    df_merged = pd.merge(df_target, df_product_attr.drop('item', axis=1), on='upc')
    df_merged = pd.merge(df_merged, df_store_demo, on='store_id')
    return df_merged


def apply_fft(df_promo):
    print 'Applying FFT'

    df_fft_result = pd.DataFrame()
    for item in df_promo.item.unique():
        for year in df_promo.year.unique():
            df_comb_split = df_promo[df_promo['year'] == int(year)][df_promo['item'] == item] \
                .sort_values(by=['promo_start_week'], ascending=[True])

            if len(df_comb_split) is 0:
                continue

            df_comb_split['fft'] = np.fft.fft(df_comb_split['total_units_sold_in_promo']).real.round()
            df_fft_result = df_fft_result.append(df_comb_split)

    cols = df_promo.columns.tolist()
    cols.append('fft')

    df_fft_result.columns = cols
    df_fft_result = df_fft_result.sort_index()
    return df_fft_result


def enrich_with_economic_data(df_target, df_gdp, df_empl, df_housing):
    for index, row in df_target.iterrows():
        state = row['state_name']
        year = int(row['year'])
        date_format = '{0}'
        try:
            gdp = int(df_gdp[df_gdp.DATE.str.startswith(date_format.format(year))][state].values[0])
            employment = int(df_empl[df_empl.DATE.str.startswith(date_format.format(year))][state].values[0])
            housing = int(df_housing[df_housing.DATE.str.startswith(date_format.format(year))][state].values[0])

            df_target.set_value(index, 'state_gdp', gdp)
            df_target.set_value(index, 'state_empl_r', employment)
            df_target.set_value(index, 'state_housing_p', housing)
        except:
            pass

    return None
